CREATE TABLE public.pets
(
    id_pet serial NOT NULL,
    name_pet character varying(100) NOT NULL,
    specie_pet character varying(100) NOT NULL,
    age_pet character varying(100) NOT NULL,
    breed_pet character varying(100) NOT NULL,
    sex_pet character varying(100) NOT NULL,
    observation character varying(500) NOT NULL,
    fk_owner integer,
    state_pet boolean NOT NULL,
    PRIMARY KEY (id_pet)
);

ALTER TABLE public.pets
    OWNER to postgres;

    INSERT INTO public.veterinarys(
	 nit_vet, name_vet, email_vet, state_vet)
	VALUES ('1111111', 'patitas', 'patitas@gmail.com', true);

INSERT INTO public.veterinarys(
	nit_vet, name_vet, email_vet, state_vet)
	VALUES ('2222222', 'suaves', 'suaves@gmail.com', true);

INSERT INTO public.branch_offices(
	address_branch, telephone_branch, image_branch, fk_vet)
	VALUES ('Cra 80', '3332121', 'img.jpg', 1);

INSERT INTO public.users(
	names_user,surnames_user,document_type_user, document_value_user, birthdate_user, department_user, city_user, neighborhood_user, telephone_user, email_user, password_user, state_user, role_user)
	VALUES ('GERMAN',
			'DELGADO JIMENEZ', 'CC', '1112496720', '1999-04-19', 'VAC', 'JAMUNDI', 'LIBERTADORES', '3163223713', 'german-1-9@hotmail.com', 'German0419', true, 'ADMIN');
;
INSERT INTO public.users(
	names_user, surnames_user, document_type_user, document_value_user, birthdate_user, department_user, city_user, neighborhood_user, telephone_user, email_user, password_user, state_user, role_user)
	VALUES ('ARI', 'VALENCIA', 'CC', '1112496721', '1999-04-12', 'VAC', 'CALI', 'SILOE', '3163223715', 'ari-13@hotmail.com', 'Ari1234', true, 'VET');
