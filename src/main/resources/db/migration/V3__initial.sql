CREATE TABLE public.Appointment
(
    id_appointment serial NOT NULL,
    hour_appointment character varying(5) NOT NULL,
    date_appointment character varying(10) NOT NULL,
    PRIMARY KEY (id_appointment)
);

ALTER TABLE public.Appointment
    OWNER to postgres;

CREATE TABLE public.clinical_history_detail
(
    id_clinical_history_detail serial NOT NULL,
    PRIMARY KEY (id_clinical_history_detail)
);

ALTER TABLE public.clinical_history_detail
    OWNER to postgres;