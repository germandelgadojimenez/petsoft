CREATE TABLE public.veterinarys
(
    id_vet serial NOT NULL,
    nit_vet character varying(20) NOT NULL,
    name_vet character varying(10) NOT NULL,
    email_vet character varying(100) NOT NULL,
    state_vet boolean NOT NULL,
    fk_owner_vet integer ,
    PRIMARY KEY (id_vet)
);

ALTER TABLE public.veterinarys
    OWNER to postgres;

    CREATE TABLE public.branch_offices
(
    id_branch serial NOT NULL,
    address_branch character varying(100) NOT NULL,
    telephone_branch character varying(100)  NOT NULL,
    image_branch character varying(100) NOT NULL,
    fk_admin_branch integer ,
    fk_vet integer ,
    PRIMARY KEY (id_branch)
);

ALTER TABLE public.branch_offices
    OWNER to postgres;

CREATE TABLE public.users
(
    id_user serial NOT NULL,
    names_user character varying(20) NOT NULL,
    surnames_user character varying(20) NOT NULL,
    document_type_user character varying(20) NOT NULL,
    document_value_user character varying(100) NOT NULL,
    birthdate_user date NOT NULL,
    department_user character varying(60) NOT NULL,
    city_user character varying(60) NOT NULL,
    neighborhood_user character varying(60) NOT NULL,
    telephone_user character varying(70) NOT NULL,
    email_user character varying(30) NOT NULL,
    password_user character varying(30) NOT NULL,
    state_user boolean NOT NULL,
    role_user character varying(20) NOT NULL,
    PRIMARY KEY (id_user)
);

ALTER TABLE public.users
    OWNER to postgres;