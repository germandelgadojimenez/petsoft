package com.practica.proyecto.converter;

import com.practica.proyecto.dto.AppointmentDTO;
import com.practica.proyecto.model.Appointment;

public class AppointmentConverter extends AbstractConverter<Appointment, AppointmentDTO>{

    @Override
    public Appointment fromDto(AppointmentDTO dto) {
        Appointment appointment = new Appointment();
        appointment.setId(dto.getIdAppointment());
        appointment.setHour(dto.getHourAppointment());
        appointment.setDate(dto.getDateAppointment());

        return appointment;
    }

    @Override
    public AppointmentDTO fromEntity(Appointment entity) {
        AppointmentDTO appointmentDTO = new AppointmentDTO();
        appointmentDTO.setIdAppointment(entity.getId());
        appointmentDTO.setHourAppointment(entity.getHour());
        appointmentDTO.setDateAppointment(entity.getDate());
        return appointmentDTO;
    }
}
