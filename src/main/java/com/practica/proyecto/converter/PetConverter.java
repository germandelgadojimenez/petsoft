package com.practica.proyecto.converter;

import com.practica.proyecto.dto.PetDTO;
import com.practica.proyecto.model.Pet;

public class PetConverter extends AbstractConverter<Pet, PetDTO> {

    public PetConverter() {
        //constructor
    }

    @Override
    public Pet fromDto(PetDTO dto) {
        Pet pet = new Pet();
        pet.setNamePet(dto.getNamePet());
        pet.setAgePet(dto.getAgePet());
        pet.setBreedPet(dto.getBreedPet());
        pet.setSexPet(dto.getSexPet());
        pet.setSpeciePet(dto.getSpeciePet());
        pet.setObservation(dto.getObservation());
        pet.setStatePet(dto.getStatePet());
        pet.setOwnerPet(dto.getOwnerPet());

        return pet;
    }

    @Override
    public PetDTO fromEntity(Pet entity) {
        PetDTO petDTO = new PetDTO();
        petDTO.setNamePet(entity.getNamePet());
        petDTO.setAgePet(entity.getAgePet());
        petDTO.setBreedPet(entity.getBreedPet());
        petDTO.setSexPet(entity.getSexPet());
        petDTO.setSpeciePet(entity.getSpeciePet());
        petDTO.setObservation(entity.getObservation());
        petDTO.setStatePet(entity.getStatePet());
        petDTO.setOwnerPet(entity.getOwnerPet());

        return petDTO;
    }
}
