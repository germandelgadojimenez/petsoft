package com.practica.proyecto.converter;

import com.practica.proyecto.dto.UserDTO;
import com.practica.proyecto.model.User;

public class UserConverter extends AbstractConverter<User, UserDTO> {

    public UserConverter() {
        //Constructor
    }

    @Override
    public User fromDto(UserDTO dto){
        User user = new User();
        user.setNames(dto.getNamesUserDto());
        user.setSurnames(dto.getSurnamesUserDto());
        user.setDocumentType(dto.getDocumentTypeUserDto());
        user.setDocumentValue(dto.getDocumentValueUserDto());
        user.setTelephone(dto.getTelephoneUserDto());
        user.setDepartment(dto.getDepartmentUserDto());
        user.setCity(dto.getCityUserDto());
        user.setNeighborhood(dto.getNeighborhoodUserDto());
        user.setBirthdate(dto.getBirthdateUserDto());
        user.setEmail(dto.getEmailUserDto());
        user.setPassword(dto.getPasswordUserDto());
        user.setState(dto.getStateUserDto());
        user.setRoleUser(dto.getRoleUserDto());

        return user;
    }

    @Override
    public UserDTO fromEntity(User entity){
        UserDTO userDTO = new UserDTO();
        userDTO.setNamesUserDto(entity.getNames());
        userDTO.setSurnamesUserDto(entity.getSurnames());
        userDTO.setDocumentTypeUserDto(entity.getDocumentType());
        userDTO.setDocumentValueUserDto(entity.getDocumentValue());
        userDTO.setTelephoneUserDto(entity.getTelephone());
        userDTO.setDepartmentUserDto(entity.getDepartment());
        userDTO.setCityUserDto(entity.getCity());
        userDTO.setNeighborhoodUserDto(entity.getNeighborhood());
        userDTO.setBirthdateUserDto(entity.getBirthdate());
        userDTO.setEmailUserDto(entity.getEmail());
        userDTO.setPasswordUserDto(entity.getPassword());
        userDTO.setStateUserDto(entity.getState());
        userDTO.setRoleUserDto(entity.getRoleUser());

        return userDTO;
    }
}
