package com.practica.proyecto.converter;

import com.practica.proyecto.dto.VeterinaryDTO;
import com.practica.proyecto.model.Veterinary;


public class VeterinaryConverter extends AbstractConverter<Veterinary, VeterinaryDTO> {

    @Override
    public Veterinary fromDto(VeterinaryDTO dto) {
        Veterinary veterinary = new Veterinary();
        veterinary.setNameVet(dto.getNameVetDTO());
        veterinary.setNitVet(dto.getNitVetDTO());
        veterinary.setEmailVet(dto.getEmailVetDTO());
        veterinary.setStateVet(dto.getStateVetDTO());
        veterinary.setOwnerVet(dto.getOwnerVetDTO());
        return veterinary;
    }

    @Override
    public VeterinaryDTO fromEntity(Veterinary entity) {
        VeterinaryDTO veterinaryDto = new VeterinaryDTO();
        veterinaryDto.setIdVet(entity.getIdVet());
        veterinaryDto.setNameVetDTO(entity.getNameVet());
        veterinaryDto.setNitVetDTO(entity.getNitVet());
        veterinaryDto.setEmailVetDTO(entity.getEmailVet());
        veterinaryDto.setStateVetDTO(entity.getStateVet());
        veterinaryDto.setOwnerVetDTO(entity.getOwnerVet());
        return veterinaryDto;
    }

    public VeterinaryConverter() {
        //Constructor
    }
}
