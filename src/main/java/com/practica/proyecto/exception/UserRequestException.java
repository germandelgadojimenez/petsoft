package com.practica.proyecto.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class UserRequestException extends RuntimeException {

    public UserRequestException(String message){
        super(message);
    }

    public UserRequestException(String message, Throwable cause){
        super(message, cause);
    }
}
