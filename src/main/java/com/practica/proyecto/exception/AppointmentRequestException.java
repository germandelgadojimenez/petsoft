package com.practica.proyecto.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class AppointmentRequestException extends RuntimeException{
    public AppointmentRequestException(String message) {
        super(message);
    }

    public AppointmentRequestException(String message, Throwable cause) {
        super(message, cause);
    }
}
