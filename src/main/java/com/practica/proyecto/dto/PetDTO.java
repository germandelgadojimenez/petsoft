package com.practica.proyecto.dto;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;

public class PetDTO {
    @Id
    @GeneratedValue(strategy =  GenerationType.IDENTITY)
    @Column(name= "id_pet")
    private Long idPet;

    @Min(value = 3, message = "Need major than six(3) chars")
    @NotEmpty(message = "Name is required")
    @Column(name = "name_pet")
    private String namePet;

    @NotEmpty(message = "specie is required")
    @Column(name = "specie_pet")
    private String speciePet;

    @NotEmpty(message = "age is required")
    @Column(name = "age_pet")
    private String agePet;

    @NotEmpty(message = "breed is required")
    @Column(name = "breed_pet")
    private String breedPet;

    @NotEmpty(message = "sex is required")
    @Column(name = "sex_pet")
    private String sexPet;

    @NotEmpty(message = "Name is required")
    @Column(name = "observation")
    private String observation;

    @NotEmpty(message = "The pet need to have an owner")
    @Column(name = "fk_owner")
    private Integer ownerPet;

    @NotEmpty(message = "state is required")
    @Column(name = "state_pet")
    private boolean statePet;

    public PetDTO() {
        //constructor
    }

    public PetDTO(PetDTO petDTO) {
        this.idPet = petDTO.idPet;
        this.namePet = petDTO.namePet;
        this.speciePet = petDTO.speciePet;
        this.agePet = petDTO.agePet;
        this.breedPet = petDTO.breedPet;
        this.sexPet = petDTO.sexPet;
        this.observation = petDTO.observation;
        this.ownerPet = petDTO.ownerPet;
        this.statePet = petDTO.statePet;
    }

    public Long getIdPet() {
        return idPet;
    }

    public void setIdPet(Long idPet) {
        this.idPet = idPet;
    }

    public String getNamePet() {
        return namePet;
    }

    public void setNamePet(String namePet) {
        this.namePet = namePet;
    }

    public String getSpeciePet() {
        return speciePet;
    }

    public void setSpeciePet(String speciePet) {
        this.speciePet = speciePet;
    }

    public String getAgePet() {
        return agePet;
    }

    public void setAgePet(String agePet) {
        this.agePet = agePet;
    }

    public String getBreedPet() {
        return breedPet;
    }

    public void setBreedPet(String breedPet) {
        this.breedPet = breedPet;
    }

    public String getSexPet() {
        return sexPet;
    }

    public void setSexPet(String sexPet) {
        this.sexPet = sexPet;
    }

    public String getObservation() {
        return observation;
    }

    public void setObservation(String observation) {
        this.observation = observation;
    }

    public Integer getOwnerPet() {
        return ownerPet;
    }

    public void setOwnerPet(Integer ownerPet) {
        this.ownerPet = ownerPet;
    }

    public boolean getStatePet() {
        return statePet;
    }

    public void setStatePet(boolean statePet) {
        this.statePet = statePet;
    }
}
