package com.practica.proyecto.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.hibernate.validator.constraints.Length;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.Email;
import javax.validation.constraints.Max;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Past;
import javax.validation.constraints.Pattern;
import java.util.Date;

public class UserDTO {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long idUserDto;

    @NotEmpty(message = "Names is required")
    @Max(value = 100, message = "Max 20 chars")
    private String namesUserDto;

    @NotEmpty(message = "Surnames is required")
    @Max(value = 100, message = "Max 20 chars")
    private String surnamesUserDto;

    @Pattern(regexp = "CC")
    @NotEmpty(message = "Document type is required")
    private String documentTypeUserDto;

    @Length(min = 5, max = 10)
    @NotEmpty(message = "Document value is required")
    private String documentValueUserDto;

    @Past
    @NotEmpty(message = "Birthdate is required")
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date birthdateUserDto;

    @NotEmpty(message = "Department is required")
    private String departmentUserDto;

    @NotEmpty(message = "City is required")
    private String cityUserDto;

    @NotEmpty(message = "Neighborhood is required")
    private String neighborhoodUserDto;

    @Length(min = 10, max = 10, message = "Telephone number have only ten(10) chars")
    @Pattern(regexp="[0-9]+", message="Telephone are only numbers")
    @NotEmpty(message = "Telephone is required")
    private String telephoneUserDto;

    @Email
    @NotEmpty(message = "Email is required")
    private String emailUserDto;

    @Pattern(regexp = "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&-+=()])(?=\\\\S+$).{8,20}$")
    @NotEmpty(message = "Password is required")
    private String passwordUserDto;

    @NotEmpty(message = "State is required")
    private Boolean stateUserDto;

    @Pattern(regexp = "[(ADMIN)(USER)(AUX)(VET)]")
    @NotEmpty(message = "Role is required")
    private String roleUserDto;

    public UserDTO() {
        //Constructor
    }

    public UserDTO(UserDTO userDTO) {
        this.idUserDto = userDTO.idUserDto;
        this.namesUserDto = userDTO.namesUserDto;
        this.surnamesUserDto = userDTO.surnamesUserDto;
        this.documentTypeUserDto = userDTO.documentTypeUserDto;
        this.documentValueUserDto = userDTO.documentValueUserDto;
        this.birthdateUserDto = userDTO.birthdateUserDto;
        this.departmentUserDto = userDTO.departmentUserDto;
        this.cityUserDto = userDTO.cityUserDto;
        this.neighborhoodUserDto = userDTO.neighborhoodUserDto;
        this.telephoneUserDto = userDTO.telephoneUserDto;
        this.emailUserDto = userDTO.emailUserDto;
        this.passwordUserDto = userDTO.passwordUserDto;
        this.stateUserDto = userDTO.stateUserDto;
        this.roleUserDto = userDTO.roleUserDto;
    }

    public Long getIdUserDto() {
        return idUserDto;
    }

    public void setIdUserDto(Long idUserDto) {
        this.idUserDto = idUserDto;
    }

    public String getNamesUserDto() {
        return namesUserDto;
    }

    public void setNamesUserDto(String namesUserDto) {
        this.namesUserDto = namesUserDto;
    }

    public String getSurnamesUserDto() {
        return surnamesUserDto;
    }

    public void setSurnamesUserDto(String surnamesUserDto) {
        this.surnamesUserDto = surnamesUserDto;
    }

    public String getDocumentTypeUserDto() {
        return documentTypeUserDto;
    }

    public void setDocumentTypeUserDto(String documentTypeUserDto) {
        this.documentTypeUserDto = documentTypeUserDto;
    }

    public String getDocumentValueUserDto() {
        return documentValueUserDto;
    }

    public void setDocumentValueUserDto(String documentValueUserDto) {
        this.documentValueUserDto = documentValueUserDto;
    }

    public Date getBirthdateUserDto() {
        return birthdateUserDto;
    }

    public void setBirthdateUserDto(Date birthdateUserDto) {
        this.birthdateUserDto = birthdateUserDto;
    }

    public String getDepartmentUserDto() {
        return departmentUserDto;
    }

    public void setDepartmentUserDto(String departmentUserDto) {
        this.departmentUserDto = departmentUserDto;
    }

    public String getCityUserDto() {
        return cityUserDto;
    }

    public void setCityUserDto(String cityUserDto) {
        this.cityUserDto = cityUserDto;
    }

    public String getNeighborhoodUserDto() {
        return neighborhoodUserDto;
    }

    public void setNeighborhoodUserDto(String neighborhoodUserDto) {
        this.neighborhoodUserDto = neighborhoodUserDto;
    }

    public String getTelephoneUserDto() {
        return telephoneUserDto;
    }

    public void setTelephoneUserDto(String telephoneUserDto) {
        this.telephoneUserDto = telephoneUserDto;
    }

    public String getEmailUserDto() {
        return emailUserDto;
    }

    public void setEmailUserDto(String emailUserDto) {
        this.emailUserDto = emailUserDto;
    }

    public String getPasswordUserDto() {
        return passwordUserDto;
    }

    public void setPasswordUserDto(String passwordUserDto) {
        this.passwordUserDto = passwordUserDto;
    }

    public Boolean getStateUserDto() {
        return stateUserDto;
    }

    public void setStateUserDto(Boolean stateUserDto) {
        this.stateUserDto = stateUserDto;
    }

    public String getRoleUserDto() {
        return roleUserDto;
    }

    public void setRoleUserDto(String roleUserDto) {
        this.roleUserDto = roleUserDto;
    }
}
