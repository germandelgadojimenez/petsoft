package com.practica.proyecto.dto;

public class AppointmentDTO {

    private Long idAppointment;
    private String hourAppointment;
    private String dateAppointment;

    public AppointmentDTO() {
        // Constructor
    }

    public AppointmentDTO(Long idAppointment, String hourAppointment, String dateAppointment) {
        this.idAppointment = idAppointment;
        this.hourAppointment = hourAppointment;
        this.dateAppointment = dateAppointment;
    }

    public Long getIdAppointment() {
        return idAppointment;
    }

    public void setIdAppointment(Long idAppointment) {
        this.idAppointment = idAppointment;
    }

    public String getHourAppointment() {
        return hourAppointment;
    }

    public void setHourAppointment(String hourAppointment) {
        this.hourAppointment = hourAppointment;
    }

    public String getDateAppointment() {
        return dateAppointment;
    }

    public void setDateAppointment(String dateAppointment) {
        this.dateAppointment = dateAppointment;
    }
}
