package com.practica.proyecto.dto;

import org.hibernate.validator.constraints.Length;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.Email;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;

public class VeterinaryDTO {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long idVet;

    @Min(value = 6, message = "Need major than six(6) chars")
    @NotEmpty(message = "Name is required")
    private String nameVetDTO;

    @Length(min = 8, max = 8, message = "Nit just have eight(8) chars")
    @Pattern(regexp="[0-9]+", message="Nit are only numbers")
    @NotEmpty(message = "Nit is required")
    private String nitVetDTO;

    @Email(message = "Email should be valid")
    @NotEmpty(message = "Email is required")
    private String emailVetDTO;

    @NotEmpty(message = "State is required")
    private Boolean stateVetDTO;

    @NotEmpty(message = "The vet need to have an owner")
    private Integer ownerVetDTO;

    public VeterinaryDTO() {
        // Constructor
    }

    public VeterinaryDTO(String nameVetDTO, String nitVetDTO, String emailVetDTO, Boolean stateVetDTO) {

        this.nameVetDTO = nameVetDTO;
        this.nitVetDTO = nitVetDTO;
        this.emailVetDTO = emailVetDTO;
        this.stateVetDTO = stateVetDTO;
        this.ownerVetDTO = null;
    }

    public Long getIdVet() {
        return idVet;
    }

    public void setIdVet(Long idVet) {
        this.idVet = idVet;
    }

    public String getNameVetDTO() {
        return nameVetDTO;
    }

    public void setNameVetDTO(String nameVetDTO) {
        this.nameVetDTO = nameVetDTO;
    }

    public String getNitVetDTO() {
        return nitVetDTO;
    }

    public void setNitVetDTO(String nitVetDTO) {
        this.nitVetDTO = nitVetDTO;
    }

    public String getEmailVetDTO() {
        return emailVetDTO;
    }

    public void setEmailVetDTO(String emailVetDTO) {
        this.emailVetDTO = emailVetDTO;
    }

    public Boolean getStateVetDTO() {
        return stateVetDTO;
    }

    public void setStateVetDTO(Boolean stateVetDTO) {
        this.stateVetDTO = stateVetDTO;
    }

    public Integer getOwnerVetDTO() {
        return ownerVetDTO;
    }

    public void setOwnerVetDTO(Integer ownerVetDTO) {
        this.ownerVetDTO = ownerVetDTO;
    }


}
