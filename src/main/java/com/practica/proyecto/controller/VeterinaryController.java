package com.practica.proyecto.controller;

import com.practica.proyecto.converter.VeterinaryConverter;
import com.practica.proyecto.dto.VeterinaryBranchOfficeDTO;
import com.practica.proyecto.dto.VeterinaryDTO;
import com.practica.proyecto.exception.VeterinaryRequestException;
import com.practica.proyecto.model.Veterinary;
import com.practica.proyecto.service.VeterinaryService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;


@RestController
@RequestMapping("/veterinary")
@Api("veterinary")
public class VeterinaryController {

    private final VeterinaryService veterinaryService;


    @Autowired
    public VeterinaryController(VeterinaryService veterinaryService) {
        this.veterinaryService = veterinaryService;
    }

    @PostMapping(path = "/save")
    @ApiOperation(value = "add veterinary", response = Veterinary.class)
    public Veterinary saveVeterinary(@Valid @RequestBody VeterinaryDTO veterinaryDTO) {
        VeterinaryConverter veterinaryConverter = new VeterinaryConverter();
        Veterinary veterinary = veterinaryConverter.fromDto(veterinaryDTO);
        return veterinaryService.saveVeterinary(veterinary);
    }

    @DeleteMapping(path = "/delete")
    @ApiOperation(value = "delete  veterinary", response = Veterinary.class)
    public void deleteVeterinary(@RequestParam(name = "id") Long id) {
        veterinaryService.deleteVeterinary(id);
    }

    @PutMapping(path = "/update")
    @ApiOperation(value = "update veterinary", response = Veterinary.class)
    public Veterinary updateVeterinary(@RequestBody @Valid VeterinaryDTO veterinaryDTO) {
        VeterinaryConverter veterinaryConverter = new VeterinaryConverter();
        Veterinary veterinary = veterinaryConverter.fromDto(veterinaryDTO);
        return veterinaryService.updateVeterinary(veterinary);
    }

    @GetMapping(path = "/all")
    @ApiOperation(value = "find all veterinaries", response = Veterinary.class)
    public List<Veterinary> findAll(){
        return veterinaryService.findAll();
    }

    @GetMapping(path = "/id")
    @ApiOperation(value = "find veterinary by id", response = Veterinary.class)
    public ResponseEntity<Veterinary> findById(@RequestParam(name = "id") Long id) {
        VeterinaryRequestException veteException = new VeterinaryRequestException("No se encontro la veterinaria");
        return ResponseEntity.ok(veterinaryService.findById(id)
                .orElseThrow(() -> veteException));
    }

    @GetMapping(path = "/veterinary/info")
    @ApiOperation(value = "find by id veterinary and branch office", response = VeterinaryBranchOfficeDTO.class)
    public ResponseEntity<VeterinaryBranchOfficeDTO> findbyIdVetSuc(@RequestParam(name = "idvet") Long idVet, @RequestParam(name = "idsuc") Long idSuc) {
        VeterinaryRequestException veteException = new VeterinaryRequestException("No se pudo encontrar la información de la veterinaria con la sucursal");
        return ResponseEntity.ok(veterinaryService.findbyIdVetSuc(idVet, idSuc)
                .orElseThrow(() -> veteException));
    }

    @GetMapping(path = "/all/state")
    @ApiOperation(value = "veterinaries active", response = Veterinary.class)
    public List<Veterinary> findAllByStateVet(@RequestParam(name = "state") Boolean state) {
        return veterinaryService.findAllByStateVet(state);
    }


}
