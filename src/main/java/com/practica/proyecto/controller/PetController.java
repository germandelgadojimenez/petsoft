package com.practica.proyecto.controller;

import com.practica.proyecto.converter.PetConverter;
import com.practica.proyecto.dto.PetDTO;
import com.practica.proyecto.exception.PetRequestException;
import com.practica.proyecto.model.Pet;
import com.practica.proyecto.service.PetService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;


@RestController
@RequestMapping("/mascota")
@Api("mascota")
public class PetController {

    private final PetService petService;

    @Autowired
    public PetController(PetService petService) {
        this.petService = petService;
    }

    @PostMapping(path = "/save")
    @ApiOperation(value = "Save pets", response = Pet.class)
    public Pet savePet(@Valid @RequestBody PetDTO petDTO) {
        PetConverter petConverter = new PetConverter();
        Pet pet = petConverter.fromDto(petDTO);
        return petService.savePet(pet);
    }

    @PutMapping(path = "/update")
    @ApiOperation(value = "Update pets", response = Pet.class)
    public Pet updatePet(@Valid @RequestBody PetDTO petDTO) {
        PetConverter petConverter = new PetConverter();
        Pet pet = petConverter.fromDto(petDTO);
        return petService.updatePet(pet);
    }

    @GetMapping(path = "/all")
    @ApiOperation(value = "Get all pets", response = Pet.class)
    public List<Pet> findAll() {
        return petService.findAll();
    }

    @GetMapping(path = "/id")
    @ApiOperation(value = "Get pet by id", response = Pet.class)
    public ResponseEntity<Pet> findById(@RequestParam(name = "idPet") Long idPet) {
        return ResponseEntity.ok(petService.findById(idPet)
                .orElseThrow(() -> new PetRequestException("No se encontro una mascota")));
    }

    @DeleteMapping(path = "/delete")
    @ApiOperation(value = "Delete pet by id", response = Pet.class)
    public void deleteMascota(@RequestParam(name = "id") Long idPet) {
        petService.deletePet(idPet);
    }

    @GetMapping(path = "/all/state")
    @ApiOperation(value = "Get pets by state", response = Pet.class)
    public List<Pet> findAllByStatePet(@RequestParam(name = "state") Boolean state) {
        return petService.findAllByStatePet(state);
    }

}





