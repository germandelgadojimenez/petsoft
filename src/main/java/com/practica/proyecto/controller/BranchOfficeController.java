package com.practica.proyecto.controller;

import com.practica.proyecto.model.BranchOffice;
import com.practica.proyecto.service.BranchOfficeService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/sucursal")
public class BranchOfficeController {
    private final BranchOfficeService branchOfficeService;

    @Autowired
    public BranchOfficeController(BranchOfficeService branchOfficeService) {
        this.branchOfficeService = branchOfficeService;
    }

    @PostMapping(path = "/save")
    public BranchOffice saveBranchOffice(@RequestBody BranchOffice branchOffice) {
        return branchOfficeService.saveBranchOffice(branchOffice);
    }

    @PutMapping(path = "/update")
    public BranchOffice updateBranchOffice(@RequestBody BranchOffice branchOffice) {
        return branchOfficeService.updateBranchOffice(branchOffice);
    }

    @GetMapping(path = "/all")
    public List<BranchOffice> findAll() {
        return branchOfficeService.findAll();
    }

    @GetMapping(path = "/id")
    public Optional<BranchOffice> findById(@RequestParam("id") Long id) {
        return branchOfficeService.findById(id);
    }

    @DeleteMapping(path = "/deleteSucursal")
    @ApiOperation(value = "elimine una sucursal", response = BranchOffice.class)
    public void deleteBranchOffice(@RequestParam(name = "id") Long id) {
        branchOfficeService.deleteBranchOffice(id);
    }


}
