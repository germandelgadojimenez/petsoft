package com.practica.proyecto.controller;

import com.practica.proyecto.converter.UserConverter;
import com.practica.proyecto.dto.UserDTO;
import com.practica.proyecto.model.User;
import com.practica.proyecto.service.UserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/usuario")
@Api("Usuario")
public class UserController {

    private final UserService userService;

    @Autowired
    public UserController(UserService userService) {
        this.userService = userService;
    }

    @PostMapping(path = "/save")
    @ApiOperation(value = "Insert Users", response = User.class)
    public User saveUser(@Valid @RequestBody UserDTO userDTO) {
        UserConverter userConverter = new UserConverter();
        User user = userConverter.fromDto(userDTO);
        return userService.saveUser(user);
    }

    @PutMapping(path = "/update")
    @ApiOperation(value = "Update Users", response = User.class)
    public User updateUser(@Valid @RequestBody UserDTO userDTO) {
        UserConverter userConverter = new UserConverter();
        User user = userConverter.fromDto(userDTO);
        return userService.updateUser(user);
    }

    @GetMapping(path = "/all/state")
    @ApiOperation(value = "Get users by state", response = User.class)
    public List<User> findAllByState(@RequestParam(name = "state") Boolean state) {
        return userService.findAllByState(state);
    }

    @GetMapping(path = "/all")
    @ApiOperation(value = "Get All Users", response = User.class)
    public List<User> findAll() {
        return userService.findAll();
    }

    @GetMapping(path = "/id")
    @ApiOperation(value = "Get user by id", response = User.class)
    public Optional<User> findById(@RequestParam(name = "id") Long id) {
        return userService.findById(id);
    }

    @DeleteMapping(path = "/delete")
    @ApiOperation(value = "Delete user by id", response = User.class)
    public void deleteById(@RequestParam(name = "id") Long id) {
        userService.deleteUser(id);
    }
}
