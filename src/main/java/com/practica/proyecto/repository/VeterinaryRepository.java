package com.practica.proyecto.repository;

import com.practica.proyecto.model.Veterinary;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface VeterinaryRepository extends JpaRepository<Veterinary, Long> {

    List<Veterinary> findAllByStateVet(Boolean state);

}
