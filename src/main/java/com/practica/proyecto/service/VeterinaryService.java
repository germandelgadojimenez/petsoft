package com.practica.proyecto.service;

import com.practica.proyecto.dto.VeterinaryBranchOfficeDTO;
import com.practica.proyecto.model.BranchOffice;
import com.practica.proyecto.model.Veterinary;
import com.practica.proyecto.repository.BranchOfficeRepository;
import com.practica.proyecto.repository.VeterinaryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class VeterinaryService {
    private final VeterinaryRepository veterinaryRepository;
    private final BranchOfficeRepository branchOfficeRepository;


    @Autowired
    public VeterinaryService(VeterinaryRepository veterinaryRepository, BranchOfficeRepository branchOfficeRepository) {
        this.veterinaryRepository = veterinaryRepository;
        this.branchOfficeRepository = branchOfficeRepository;
    }

    public Veterinary saveVeterinary(Veterinary veterinary) {
        return veterinaryRepository.save(veterinary);
    }

    public Veterinary updateVeterinary(Veterinary veterinary) {
        return veterinaryRepository.save(veterinary);
    }

    public Optional<Veterinary> findById(Long id) {
        return veterinaryRepository.findById(id);
    }

    public List<Veterinary> findAll() {
        return veterinaryRepository.findAll();
    }

    public Optional<VeterinaryBranchOfficeDTO> findbyIdVetSuc(Long idVet, Long idSuc) {
        VeterinaryBranchOfficeDTO vetDto = new VeterinaryBranchOfficeDTO();
        Optional<Veterinary> vet = veterinaryRepository.findById(idVet);
        Optional<BranchOffice> suc = branchOfficeRepository.findById(idSuc);
        if (vet.isPresent() && suc.isPresent()) {
            vetDto.setNit(vet.get().getNitVet());
            vetDto.setNombreVet(vet.get().getNameVet());
            vetDto.setCorreo(vet.get().getEmailVet());
            vetDto.setDireccion(suc.get().getAddressBranch());
            vetDto.setTelefono(suc.get().getTelephoneBranch());
            vetDto.setImagen(suc.get().getImageBranch());

        }
        return Optional.of(vetDto);

    }

    public void deleteVeterinary(Long id) {
        veterinaryRepository.deleteById(id);
    }

    public List<Veterinary> findAllByStateVet(Boolean state) {
        return veterinaryRepository.findAllByStateVet(state);
    }


}
