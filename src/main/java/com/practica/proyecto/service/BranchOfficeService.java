package com.practica.proyecto.service;

import com.practica.proyecto.model.BranchOffice;
import com.practica.proyecto.repository.BranchOfficeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class BranchOfficeService {

    private final BranchOfficeRepository branchOfficeRepository;

    @Autowired
    public BranchOfficeService(BranchOfficeRepository branchOfficeRepository) {
        this.branchOfficeRepository = branchOfficeRepository;
    }

    public BranchOffice saveBranchOffice(BranchOffice branchOffice) {
        return branchOfficeRepository.save(branchOffice);
    }

    public BranchOffice updateBranchOffice(BranchOffice branchOffice) {
        return branchOfficeRepository.save(branchOffice);
    }

    public Optional<BranchOffice> findById(Long id) {
        return branchOfficeRepository.findById(id);
    }

    public List<BranchOffice> findAll() {
        return branchOfficeRepository.findAll();
    }

    public void deleteBranchOffice(Long id) {
        branchOfficeRepository.deleteById(id);
    }
}
