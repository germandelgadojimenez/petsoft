package com.practica.proyecto.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "clinical_history_detail")
public class ClinicalHistoryDetail {

    @Id
    @GeneratedValue(strategy =  GenerationType.IDENTITY)
    @Column(name= "id_detail")
    private Long idDetail;

    public ClinicalHistoryDetail(Long idDetail) {
        this.idDetail = idDetail;
    }

    public ClinicalHistoryDetail() {
        // constructor
    }

    public Long getIdDetail() {
        return idDetail;
    }

    public void setIdDetail(Long idDetail) {
        this.idDetail = idDetail;
    }
}
