package com.practica.proyecto.model;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "veterinarys")
public class Veterinary {

    @Id
    @GeneratedValue(strategy =  GenerationType.IDENTITY)
    @Column(name= "id_vet")
    private Long idVet;

    @Column(name = "name_vet")
    private String nameVet;


    @Column(name = "nit_vet")
    private String nitVet;

    @Column(name = "email_vet")
    private String emailVet;

    @Column(name= "state_vet")
    private Boolean stateVet;

    @Column(name = "fk_owner_vet")
    private Integer ownerVet;


    public Veterinary(Long idVet, String nameVet, String nitVet, String emailVet, Boolean stateVet, Integer ownerVet) {
        this.idVet = idVet;
        this.nameVet = nameVet;
        this.nitVet = nitVet;
        this.emailVet = emailVet;
        this.stateVet = stateVet;
        this.ownerVet = ownerVet;
    }

    public Veterinary() {
        // Constructor
    }

    public Long getIdVet() {
        return idVet;
    }

    public void setIdVet(Long idVet) {
        this.idVet = idVet;
    }

    public String getNameVet() {
        return nameVet;
    }

    public void setNameVet(String nameVet) {
        this.nameVet = nameVet;
    }

    public String getNitVet() {
        return nitVet;
    }

    public void setNitVet(String nitVet) {
        this.nitVet = nitVet;
    }

    public String getEmailVet() {
        return emailVet;
    }

    public void setEmailVet(String emailVet) {
        this.emailVet = emailVet;
    }

    public Boolean getStateVet() {
        return stateVet;
    }

    public void setStateVet(Boolean stateVet) {
        this.stateVet = stateVet;
    }

    public Integer getOwnerVet() {
        return ownerVet;
    }

    public void setOwnerVet(Integer ownerVet) {
        this.ownerVet = ownerVet;
    }

}
