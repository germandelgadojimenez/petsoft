package com.practica.proyecto.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;


@Entity
@Table(name = "users")
public class User {

    @Id
    @GeneratedValue(strategy =  GenerationType.IDENTITY)
    @Column(name = "id_user")
    private Long id;

    @Column(name = "names_user")
    private String names;

    @Column(name = "surnames_user")
    private String surnames;

    @Column(name = "document_type_user")
    private String documentType;

    @Column(name = "document_value_user")
    private String documentValue;

    @Column(name = "birthdate_user")
    private Date birthdate;

    @Column(name = "department_user")
    private String department;

    @Column(name = "city_user")
    private String city;

    @Column(name = "neighborhood_user")
    private String neighborhood;

    @Column(name = "telephone_user")
    private String telephone;

    @Column(name = "email_user")
    private String email;

    @Column(name = "password_user")
    private String password;

    @Column(name = "state_user")
    private Boolean state;

    @Column( name = "role_user")
    private String roleUser;


    public User() {
        // Constructor
    }

    public User(User user) {
        this.id = user.id;
        this.names = user.names;
        this.surnames = user.surnames;
        this.documentType = user.documentType;
        this.documentValue = user.documentValue;
        this.birthdate = user.birthdate;
        this.department = user.department;
        this.city = user.city;
        this.neighborhood = user.neighborhood;
        this.telephone = user.telephone;
        this.email = user.email;
        this.password = user.password;
        this.state = user.state;
        this.roleUser = user.roleUser;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNames() {
        return names;
    }

    public void setNames(String names) {
        this.names = names;
    }

    public String getSurnames() {
        return surnames;
    }

    public void setSurnames(String surnames) {
        this.surnames = surnames;
    }

    public String getDocumentType() {
        return documentType;
    }

    public void setDocumentType(String documentType) {
        this.documentType = documentType;
    }

    public String getDocumentValue() {
        return documentValue;
    }

    public void setDocumentValue(String documentValue) {
        this.documentValue = documentValue;
    }

    public Date getBirthdate() {
        return birthdate;
    }

    public void setBirthdate(Date birthdate) {
        this.birthdate = birthdate;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getNeighborhood() {
        return neighborhood;
    }

    public void setNeighborhood(String neighborhood) {
        this.neighborhood = neighborhood;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Boolean getState() {
        return state;
    }

    public void setState(Boolean state) {
        this.state = state;
    }

    public String getRoleUser() {
        return roleUser;
    }

    public void setRoleUser(String roleUser) {
        this.roleUser = roleUser;
    }


}
