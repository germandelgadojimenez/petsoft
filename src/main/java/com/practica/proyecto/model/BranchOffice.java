package com.practica.proyecto.model;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "branch_offices")
public class BranchOffice {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_branch")
    private Long idBranch;

    @Column(name = "address_branch")
    private String addressBranch;

    @Column(name = "telephone_branch")
    private String telephoneBranch;

    @Column(name = "image_branch")
    private String imageBranch;

    @Column(name = "fk_admin_branch")
    private Long fkAdmin;


    public BranchOffice(Long idBranch, String addressBranch, String telephoneBranch, String imageBranch) {
        this.idBranch = idBranch;
        this.addressBranch = addressBranch;
        this.telephoneBranch = telephoneBranch;
        this.imageBranch = imageBranch;
        this.fkAdmin = null;
    }

    public BranchOffice() {
    }

    public Long getIdBranch() {
        return idBranch;
    }

    public void setIdBranch(Long idSuc) {
        this.idBranch = idSuc;
    }

    public String getAddressBranch() {
        return addressBranch;
    }

    public void setAddressBranch(String addressBranch) {
        this.addressBranch = addressBranch;
    }

    public String getTelephoneBranch() {
        return telephoneBranch;
    }

    public void setTelephoneBranch(String telephoneBranch) {
        this.telephoneBranch = telephoneBranch;
    }

    public String getImageBranch() {
        return imageBranch;
    }

    public void setImageBranch(String imageBranch) {
        this.imageBranch = imageBranch;
    }

    public Long getFkAdmin() {
        return fkAdmin;
    }

    public void setFkAdmin(Long fkAdmin) {
        this.fkAdmin = fkAdmin;
    }


}
