package com.practica.proyecto.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "pets")
public class Pet {

    @Id
    @GeneratedValue(strategy =  GenerationType.IDENTITY)
    @Column(name= "id_pet")
    private Long idPet;

    @Column(name = "name_pet")
    private String namePet;

    @Column(name = "specie_pet")
    private String speciePet;

    @Column(name = "age_pet")
    private String agePet;

    @Column(name = "breed_pet")
    private String breedPet;

    @Column(name = "sex_pet")
    private String sexPet;

    @Column(name = "observation")
    private String observation;

    @Column(name = "fk_owner")
    private Integer ownerPet;

    @Column(name = "state_pet")
    private boolean statePet;


    public Pet() {
        // Constructor
    }

    public Pet(Pet pet) {
        this.idPet = pet.idPet;
        this.namePet = pet.namePet;
        this.speciePet = pet.speciePet;
        this.agePet = pet.agePet;
        this.breedPet = pet.breedPet;
        this.sexPet = pet.sexPet;
        this.observation = pet.observation;
        this.statePet = pet.statePet;
    }

    public Long getIdPet() {
        return idPet;
    }

    public void setIdPet(Long idMascota) {
        this.idPet = idMascota;
    }

    public String getNamePet() {
        return namePet;
    }

    public void setNamePet(String nombreMascota) {
        this.namePet = nombreMascota;
    }

    public String getSpeciePet() {
        return speciePet;
    }

    public void setSpeciePet(String especieMascota) {
        this.speciePet = especieMascota;
    }

    public String getAgePet() {
        return agePet;
    }

    public void setAgePet(String edadMascota) {
        this.agePet = edadMascota;
    }

    public String getBreedPet() {
        return breedPet;
    }

    public void setBreedPet(String razaMascota) {
        this.breedPet = razaMascota;
    }

    public String getSexPet() {
        return sexPet;
    }

    public void setSexPet(String sexoMascota) {
        this.sexPet = sexoMascota;
    }

    public String getObservation() {
        return observation;
    }

    public void setObservation(String observaciones) {
        this.observation = observaciones;
    }

    public Integer getOwnerPet() {
        return ownerPet;
    }

    public void setOwnerPet(Integer ownerPet) {
        this.ownerPet = ownerPet;
    }

    public boolean getStatePet() {
        return statePet;
    }

    public void setStatePet(boolean estadoMascota) {
        this.statePet = estadoMascota;
    }
}
