package com.practica.proyecto.factory;

import com.practica.proyecto.dto.VeterinaryBranchOfficeDTO;

public class VeterinaryBranchOfficeDTOFactory implements InstanceEntity<VeterinaryBranchOfficeDTO> {


    private static final String NAME_VET = "Petties";
    private static final String NIT = "111111111";
    private static final String EMAIL_VET = "patitas@gmail.com";
    private static final String TELEPHONE_BRANCH_OFFICE = "4454341";
    private static final String IMAGE_BRANCH_OFFICE = "foto.jpg";

    @Override
    public VeterinaryBranchOfficeDTO createEntity() {

        VeterinaryBranchOfficeDTO veterinaryBranchOfficeDTO = new VeterinaryBranchOfficeDTO();
        veterinaryBranchOfficeDTO.setNombreVet(NAME_VET);
        veterinaryBranchOfficeDTO.setNit(NIT);
        veterinaryBranchOfficeDTO.setCorreo(EMAIL_VET);
        veterinaryBranchOfficeDTO.setTelefono(TELEPHONE_BRANCH_OFFICE);
        veterinaryBranchOfficeDTO.setImagen(IMAGE_BRANCH_OFFICE);
        return veterinaryBranchOfficeDTO;
    }
}
