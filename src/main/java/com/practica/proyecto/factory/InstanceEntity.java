package com.practica.proyecto.factory;

public interface InstanceEntity<E> {
    public E createEntity();
}
