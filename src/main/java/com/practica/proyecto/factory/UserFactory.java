package com.practica.proyecto.factory;

import com.practica.proyecto.model.User;

import java.util.Date;

public class UserFactory implements InstanceEntity<User> {

    private static final Long ID_USER = Long.valueOf(1);
    private static final String NAMES = "GERMAN";
    private static final String SURNAMES = "DELGADO JIMENEZ";
    private static final String DOCUMENT_TYPE = "CC";
    private static final String DOCUMENT_VALUE = "1112496720";
    private static final Date BIRTH_DATE = new Date(1999, 04, 19);
    private static final String DEPARTMENT = "VAC";
    private static final String CITY = "JAMUNDI";
    private static final String NEIGHBORHOOD = "LIBERTADORES";
    private static final String TELEPHONE = "3163223713";
    private static final String EMAIL = "german-1-9@hotmail.com";
    private static final String PASSWORD = "German0419";
    private static final Boolean STATE = true;
    private static final String ROLE = "ADMIN";

    @Override
    public User createEntity() {
        User user = new User();
        user.setId(ID_USER);
        user.setNames(NAMES);
        user.setSurnames(SURNAMES);
        user.setDocumentType(DOCUMENT_TYPE);
        user.setDocumentValue(DOCUMENT_VALUE);
        user.setBirthdate(BIRTH_DATE);
        user.setDepartment(DEPARTMENT);
        user.setCity(CITY);
        user.setNeighborhood(NEIGHBORHOOD);
        user.setTelephone(TELEPHONE);
        user.setEmail(EMAIL);
        user.setPassword(PASSWORD);
        user.setState(STATE);
        user.setRoleUser(ROLE);

        return user;
    }
}
