package com.practica.proyecto.factory;

import com.practica.proyecto.dto.UserDTO;

import java.util.Date;

public class UserDTOFactory implements InstanceEntity<UserDTO> {

    private static final Long ID_USER = Long.valueOf(1);
    private static final String NAMES = "ARI";
    private static final String SURNAMES = "VALENCIA CEBALLOS";
    private static final String DOCUMENT_TYPE = "CC";
    private static final String DOCUMENT_VALUE = "1112496700";
    private static final Date BIRTH_DATE = new Date(1999, 12, 21);
    private static final String DEPARTMENT = "VAC";
    private static final String CITY = "CALI";
    private static final String NEIGHBORHOOD = "SILOE";
    private static final String TELEPHONE = "3163223712";
    private static final String EMAIL = "arivalencia@hotmail.com";
    private static final String PASSWORD = "Ari12345*";
    private static final Boolean STATE = false;
    private static final String ROLE = "AUX";

    @Override
    public UserDTO createEntity(){
        UserDTO userDTO = new UserDTO();
        userDTO.setIdUserDto(ID_USER);
        userDTO.setNamesUserDto(NAMES);
        userDTO.setSurnamesUserDto(SURNAMES);
        userDTO.setDocumentTypeUserDto(DOCUMENT_TYPE);
        userDTO.setDocumentValueUserDto(DOCUMENT_VALUE);
        userDTO.setBirthdateUserDto(BIRTH_DATE);
        userDTO.setDepartmentUserDto(DEPARTMENT);
        userDTO.setCityUserDto(CITY);
        userDTO.setNeighborhoodUserDto(NEIGHBORHOOD);
        userDTO.setTelephoneUserDto(TELEPHONE);
        userDTO.setEmailUserDto(EMAIL);
        userDTO.setPasswordUserDto(PASSWORD);
        userDTO.setStateUserDto(STATE);
        userDTO.setRoleUserDto(ROLE);

        return userDTO;
    }
}
