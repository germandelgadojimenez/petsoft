package com.practica.proyecto.factory;

import com.practica.proyecto.model.Veterinary;

public class VeterinaryFactory implements InstanceEntity<Veterinary> {

    private static final Long ID = Long.valueOf(1);
    private static final int ID_OWNER_VET = 1;
    private static final String NAME_VET = "pet and love";
    private static final String NIT = "111111111";
    private static final String EMAIL_VET = "patitas@gmail.com";
    private static final Boolean STATE_VET = true;

    @Override
    public Veterinary createEntity() {
        Veterinary veterinary = new Veterinary();
        veterinary.setIdVet(ID);
        veterinary.setNameVet(NAME_VET);
        veterinary.setNitVet(NIT);
        veterinary.setOwnerVet(ID_OWNER_VET);
        veterinary.setEmailVet(EMAIL_VET);
        veterinary.setStateVet(STATE_VET);

        return veterinary;
    }

}
