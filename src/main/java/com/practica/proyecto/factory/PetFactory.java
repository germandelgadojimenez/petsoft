package com.practica.proyecto.factory;

import com.practica.proyecto.model.Pet;

public class PetFactory implements InstanceEntity<Pet>{

    private static final Long IDPET = Long.valueOf(2);
    private static final String NAME = "skylie2";
    private static final String SPECIE = "skylie2";
    private static final String AGE = "3";
    private static final String BREED = "criollo";
    private static final String SEX = "hembra";
    private static final String OBSERVATIONS = "blanco";
    private static final Boolean STATE = true;

    @Override
    public Pet createEntity() {
        Pet pet = new Pet();
        pet.setIdPet(IDPET);
        pet.setNamePet(NAME);
        pet.setSpeciePet(SPECIE);
        pet.setAgePet(AGE);
        pet.setBreedPet(BREED);
        pet.setSexPet(SEX);
        pet.setObservation(OBSERVATIONS);
        pet.setStatePet(STATE);

        return pet;
    }
}
