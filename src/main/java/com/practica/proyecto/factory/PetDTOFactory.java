package com.practica.proyecto.factory;

import com.practica.proyecto.dto.PetDTO;

public class PetDTOFactory implements InstanceEntity <PetDTO> {

    private static final Long IDPET = Long.valueOf(2);
    private static final String NAME = "toby";
    private static final String SPECIE = "canino";
    private static final String AGE = "6";
    private static final String BREED = "pinche";
    private static final String SEX = "macho";
    private static final String OBSERVATIONS = "cafe";
    private static final Boolean STATE = true;

    @Override
    public PetDTO createEntity() {
        PetDTO petDTO = new PetDTO();
        petDTO.setIdPet(IDPET);
        petDTO.setNamePet(NAME);
        petDTO.setSpeciePet(SPECIE);
        petDTO.setAgePet(AGE);
        petDTO.setBreedPet(BREED);
        petDTO.setSexPet(SEX);
        petDTO.setObservation(OBSERVATIONS);
        petDTO.setStatePet(STATE);

        return petDTO;
    }
}
