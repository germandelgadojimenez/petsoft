package com.practica.proyecto.factory;

import com.practica.proyecto.model.BranchOffice;

public class BranchOfficeFactory implements InstanceEntity<BranchOffice> {

    private static final Long ID_BRANCH_OFFICE = Long.valueOf(1);
    private static final Long ID_ADMIN = Long.valueOf(1);
    private static final String ADDRESS = "CALLE 15 #200-16";
    private static final String TELEPHONE = "4454341";
    private static final String IMAGE = "foto.jpg";

    @Override
    public BranchOffice createEntity(){
        BranchOffice branchOffice = new BranchOffice();
        branchOffice.setIdBranch(ID_BRANCH_OFFICE);
        branchOffice.setAddressBranch(ADDRESS);
        branchOffice.setImageBranch(IMAGE);
        branchOffice.setTelephoneBranch(TELEPHONE);
        branchOffice.setFkAdmin(ID_ADMIN);

        return branchOffice;
    }
}
