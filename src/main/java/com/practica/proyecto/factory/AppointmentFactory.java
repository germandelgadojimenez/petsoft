package com.practica.proyecto.factory;

import com.practica.proyecto.model.Appointment;

public class AppointmentFactory implements InstanceEntity<Appointment> {

    public final long ID = Long.valueOf(1);
    private static final String HOUR = "13:00";
    private static final String DATE = "10/10/2020";

    @Override
    public Appointment createEntity() {
        Appointment appointment = new Appointment();
        appointment.setId(ID);
        appointment.setDate(HOUR);
        appointment.setHour(DATE);
        return appointment;
    }
}
