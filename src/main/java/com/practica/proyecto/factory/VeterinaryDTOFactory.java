package com.practica.proyecto.factory;

import com.practica.proyecto.dto.VeterinaryDTO;

public class VeterinaryDTOFactory implements InstanceEntity<VeterinaryDTO> {

    private static final Long ID = Long.valueOf(1);
    private static final int ID_OWNER_VET = 1;
    private static final String NAME_VET = "pet and love";
    private static final String NIT = "111111111";
    private static final String EMAIL_VET = "patitas@gmail.com";
    private static final Boolean STATE_VET = true;

    @Override
    public VeterinaryDTO createEntity() {
        VeterinaryDTO veterinaryDTO = new VeterinaryDTO();
        veterinaryDTO.setIdVet(ID);
        veterinaryDTO.setNameVetDTO(NAME_VET);
        veterinaryDTO.setNitVetDTO(NIT);
        veterinaryDTO.setOwnerVetDTO(ID_OWNER_VET);
        veterinaryDTO.setEmailVetDTO(EMAIL_VET);
        veterinaryDTO.setStateVetDTO(STATE_VET);

        return veterinaryDTO;
    }

}
