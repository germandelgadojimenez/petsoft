package com.practica.proyecto.factory;

import com.practica.proyecto.dto.PetDTO;
import com.practica.proyecto.dto.UserDTO;
import com.practica.proyecto.dto.VeterinaryBranchOfficeDTO;
import com.practica.proyecto.model.Appointment;
import com.practica.proyecto.model.BranchOffice;
import com.practica.proyecto.model.Pet;
import com.practica.proyecto.model.User;
import com.practica.proyecto.model.Veterinary;

public class EntityFactory {

    private EntityFactory() {
        //Constructor
    }

    public static InstanceEntity<Veterinary> createVeterinary() {
        return new VeterinaryFactory();
    }

    public static VeterinaryDTOFactory createVeterinaryDTO() {
        return new VeterinaryDTOFactory();
    }

    public static InstanceEntity<VeterinaryBranchOfficeDTO> createVeterinaryBranchDTO() {
        return new VeterinaryBranchOfficeDTOFactory();
    }

    public static InstanceEntity<BranchOffice> createBranchOffice(){
        return new BranchOfficeFactory();
    }

    public static InstanceEntity<User> createUser(){
        return new UserFactory();
    }

    public static InstanceEntity<UserDTO> createUserDTO(){
        return new UserDTOFactory();
    }

    public static InstanceEntity<Pet> createPet(){
        return new PetFactory();
    }

    public static InstanceEntity<PetDTO> createPetDTO() {
        return new PetDTOFactory();
    }

    public static InstanceEntity<Appointment> createAppointment(){
        return new AppointmentFactory();
    }
}
