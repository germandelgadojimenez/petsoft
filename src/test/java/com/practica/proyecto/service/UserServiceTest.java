package com.practica.proyecto.service;

import com.practica.proyecto.factory.EntityFactory;
import com.practica.proyecto.factory.InstanceEntity;
import com.practica.proyecto.model.User;
import com.practica.proyecto.repository.UserRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.mock;

class UserServiceTest {

    private User user;
    private InstanceEntity<User> userInstanceEntity = EntityFactory.createUser();
    private Optional<User> optionalUsuario;

    @Mock
    private UserRepository userRepository;

    @InjectMocks
    UserService userService;

    @BeforeEach
    public void init() {
        user = mock(User.class);
        user = userInstanceEntity.createEntity();
        optionalUsuario = Optional.of(user);
        MockitoAnnotations.initMocks(this);
    }

    @Test
    void findAllByStateTest() {
        final int num = 1;
        final User user = new User();
        Mockito.when(userRepository.findAll()).thenReturn(Arrays.asList(user));
        final List<User> resp = userService.findAll();

        assertNotNull(resp);
        assertFalse(resp.isEmpty());
        assertEquals(resp.size(), num);
    }

    @Test
    void findAll() {
        final int num = 1;
        final User user = new User();
        Mockito.when(userRepository.findAllByState(true)).thenReturn(Arrays.asList(user));
        final List<User> resp = userService.findAllByState(true);

        assertNotNull(resp);
        assertFalse(resp.isEmpty());
        assertEquals(resp.size(), num);
    }

    @Test
    void deleteUserTest() {
        final int num = 1;
        Mockito.when(userRepository.findById(user.getId())).thenReturn(optionalUsuario);
        userRepository.deleteById(user.getId());
        Mockito.verify(userRepository, Mockito.times(num)).deleteById(user.getId());
    }

    @Test
    void saveUserTest() {
        Mockito.when(userRepository.save(user)).thenReturn(user);
        User userSave = userService.saveUser(user);

        assertNotNull(userSave);
    }

    @Test
    void updateUserTest() {
        Mockito.when(userRepository.save(user)).thenReturn(user);
        User userSave = userService.updateUser(user);

        assertNotNull(userSave);
        assertEquals(user, userSave);
    }

    @Test
    void findByIdTest() {
        Mockito.when(userRepository.findById(user.getId())).thenReturn(optionalUsuario);
        userService.findById(user.getId());
    }

    @Test
    void deleteUser() {
        Mockito.when(userRepository.findById(user.getId())).thenReturn(optionalUsuario);
        userService.deleteUser(user.getId());
    }


}
