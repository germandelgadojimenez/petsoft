package com.practica.proyecto.service;

import com.practica.proyecto.model.BranchOffice;
import com.practica.proyecto.repository.BranchOfficeRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.mock;

class BranchOfficeServiceTest {

    private static final Long ID_BRANCH_OFFICE = Long.valueOf(1);
    private static final String ADDRESS_BRANCH_OFFICE = "calle busquela con carrera encuentrela";
    private static final String TELEPHONE_BRANCH_OFFICE = "31454";
    private static final String IMAGE_BRANCH_OFFICE = "LOL.PNG";

    private BranchOffice branchOffice;
    private Optional<BranchOffice> optionalBranchOffice;


    @Mock
    private BranchOfficeRepository branchOfficeRepository;

    @InjectMocks
    private BranchOfficeService branchOfficeService;

    @BeforeEach
    public void init() {
        branchOffice = mock(BranchOffice.class);
        optionalBranchOffice = Optional.of(branchOffice);

        MockitoAnnotations.initMocks(this);
        branchOffice.setIdBranch(ID_BRANCH_OFFICE);
        branchOffice.setAddressBranch(ADDRESS_BRANCH_OFFICE);
        branchOffice.setTelephoneBranch(TELEPHONE_BRANCH_OFFICE);
        branchOffice.setImageBranch(IMAGE_BRANCH_OFFICE);
    }

    @Test
    void saveBranchOfficeTest() {
        Mockito.when(branchOfficeRepository.save(branchOffice)).thenReturn(branchOffice);
        BranchOffice branchOffice = branchOfficeService.saveBranchOffice(this.branchOffice);

        assertNotNull(branchOffice);
    }

    @Test
    void updateBranchOfficeTest() {
        Mockito.when(branchOfficeRepository.save(branchOffice)).thenReturn(branchOffice);
        BranchOffice branchOffice = branchOfficeService.updateBranchOffice(this.branchOffice);

        assertNotNull(branchOffice);
        assertEquals(this.branchOffice, branchOffice);
    }

    @Test
    void findByAllTest() {
        final BranchOffice branchOffice = new BranchOffice();
        Mockito.when(branchOfficeRepository.findAll()).thenReturn(Arrays.asList(branchOffice));
        final List<BranchOffice> resp = branchOfficeService.findAll();

        assertNotNull(resp);
        assertFalse(resp.isEmpty());
        assertEquals(resp.size(), 1);
    }

    @Test
    void deleteBranchOfficeTest() {
        Mockito.when(branchOfficeRepository.findById(ID_BRANCH_OFFICE)).thenReturn(optionalBranchOffice);
        branchOfficeService.deleteBranchOffice(ID_BRANCH_OFFICE);
    }

    @Test
    void findByIdTest() {
        Mockito.when(branchOfficeRepository.findById(ID_BRANCH_OFFICE)).thenReturn(optionalBranchOffice);
        branchOfficeService.findById(ID_BRANCH_OFFICE);
    }

}
