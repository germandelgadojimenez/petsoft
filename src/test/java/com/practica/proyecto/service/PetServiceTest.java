package com.practica.proyecto.service;

import com.practica.proyecto.model.Pet;
import com.practica.proyecto.repository.PetRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.mock;

class PetServiceTest {

    private static final Long ID = Long.valueOf(1);
    private static final String NAME = "skylie";
    private static final String SPECIE = "skylie";
    private static final String AGE = "2";
    private static final String BREED = "chriollo";
    private static final String SEX = "macho";
    private static final String OBSERVATIONS = "negro";
    private static final Boolean STATE = true;

    private Pet pet;
    private Optional<Pet> optionalPet;

    @Mock
    private PetRepository petRepository;

    @InjectMocks
    PetService petService;

    @BeforeEach
    public void init() {
        pet = mock(Pet.class);
        optionalPet = Optional.of(pet);
        MockitoAnnotations.initMocks(this);
        pet.setNamePet(NAME);
        pet.setSpeciePet(SPECIE);
        pet.setAgePet(AGE);
        pet.setBreedPet(BREED);
        pet.setSexPet(SEX);
        pet.setObservation(OBSERVATIONS);
        pet.setStatePet(STATE);
    }

    @Test
    void findByIdTest() {
        Mockito.when(petRepository.findById(ID)).thenReturn(optionalPet);
        petService.findById(ID);
    }

    @Test
    void saveMascota() {
        Mockito.when(petRepository.save(pet)).thenReturn(pet);
        Pet petSave = petService.savePet(pet);

        assertNotNull(petSave);
    }

    @Test
    void updateMascota() {
        Mockito.when(petRepository.save(pet)).thenReturn(pet);
        Pet petUpdate = petService.updatePet(pet);

        assertNotNull(petUpdate);
        assertEquals(pet, petUpdate);
    }

    @Test
    void findAllByStatePetTest() {
        final int number = 1;
        final Pet pet = new Pet();
        Mockito.when(petRepository.findAllByStatePet(true)).thenReturn(Arrays.asList(pet));
        final List<Pet> resp = petService.findAllByStatePet(true);

        assertNotNull(resp);
        assertFalse(resp.isEmpty());
        assertEquals(resp.size(),number);
    }

    @Test
    void findByAllTest() {
        final int number = 1;
        final Pet pet = new Pet();
        Mockito.when(petRepository.findAll()).thenReturn(Arrays.asList(pet));
        final List<Pet> resp = petService.findAll();

        assertNotNull(resp);
        assertFalse(resp.isEmpty());
        assertEquals(resp.size(), number);
    }


    @Test
    void deleteMascotaTest() {
        Mockito.when(petRepository.findById(ID)).thenReturn(optionalPet);
        petService.deletePet(ID);
    }


}
