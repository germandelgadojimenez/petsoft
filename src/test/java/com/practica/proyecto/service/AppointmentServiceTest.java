package com.practica.proyecto.service;

import com.practica.proyecto.model.Appointment;
import com.practica.proyecto.repository.AppointmentRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.mock;

class AppointmentServiceTest {

    public final long ID = Long.valueOf(1);
    private static final String HOUR = "13:00";
    private static final String DATE = "10/10/2020";

    private Appointment appointment;
    private Optional<Appointment> appointmentOptional;

    @Mock
    private AppointmentRepository appointmentRepository;

    @InjectMocks
    private AppointmentService appointmentService;

    @BeforeEach
    public void init(){
        appointment = mock(Appointment.class);
        appointmentOptional = Optional.of(appointment);

        MockitoAnnotations.initMocks(this);
        appointment.setId(ID);
        appointment.setHour(HOUR);
        appointment.setDate(DATE);
    }

    @Test
    void saveCitaTest(){
        Mockito.when(appointmentRepository.save(appointment)).thenReturn(appointment);
        Appointment appointmentSave = appointmentService.saveAppointment(appointment);

        assertNotNull(appointmentSave);
    }

    @Test
    void updateCitaTest() {
        Mockito.when(appointmentRepository.save(appointment)).thenReturn(appointment);
        Appointment appointmentUpdate = appointmentService.updateAppointment(appointment);

        assertNotNull(appointmentUpdate);
        assertEquals(appointment, appointmentUpdate);
    }

    @Test
    void findByAllTest () {
        final int number = 1;
        Mockito.when(appointmentRepository.findAll()).thenReturn(Arrays.asList(appointment));
        final List<Appointment> appointmentList = appointmentService.findAll();

        assertNotNull(appointmentList);
        assertFalse(appointmentList.isEmpty());
        assertEquals(appointmentList.size(), number);
    }

    @Test
    void deleteAppointment() {
        Mockito.when(appointmentRepository.findById(ID)).thenReturn(appointmentOptional);
        appointmentService.deleteAppointment(ID);
    }

    @Test
    void findByIdTest() {
        Mockito.when(appointmentRepository.findById(ID)).thenReturn(appointmentOptional);
        appointmentService.findById(ID);
    }
}
