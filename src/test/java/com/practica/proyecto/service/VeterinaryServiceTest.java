package com.practica.proyecto.service;


import com.practica.proyecto.dto.VeterinaryBranchOfficeDTO;
import com.practica.proyecto.factory.EntityFactory;
import com.practica.proyecto.factory.InstanceEntity;
import com.practica.proyecto.model.BranchOffice;
import com.practica.proyecto.model.Veterinary;
import com.practica.proyecto.repository.BranchOfficeRepository;
import com.practica.proyecto.repository.VeterinaryRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.mock;

class VeterinaryServiceTest {

    private static final Long ID_BRANCH_OFFICE = Long.valueOf(1);
    private static final String ADDRESS_BRANCH_OFFICE = "CRA 80";
    private static final String TELEPHONE_BRANCH_OFFICE = "4454341";
    private static final String IMAGE_BRANCH_OFFICE = "foto.jpg";

    private static final Long ID = Long.valueOf(1);

    private InstanceEntity<Veterinary> instanceEntityV = EntityFactory.createVeterinary();
    private InstanceEntity<VeterinaryBranchOfficeDTO> instanceEntityVBDTO = EntityFactory.createVeterinaryBranchDTO();
    private Veterinary veterinary;
    private Veterinary vet;
    private BranchOffice branchOffice;
    private VeterinaryBranchOfficeDTO veterinaryBranchOfficeDTO;
    private Optional<Veterinary> veterinaryOptional;
    private Optional<BranchOffice> branchOfficeOptional;
    private Optional<VeterinaryBranchOfficeDTO> veterinaryBranchOfficeDTOOptional;

    @Mock
    private VeterinaryRepository veterinaryRepository;

    @Mock
    private BranchOfficeRepository branchOfficeRepository;

    @InjectMocks
    VeterinaryService veterinaryService;

    @BeforeEach
    public void init() {
        veterinary = mock(Veterinary.class);
        veterinary = instanceEntityV.createEntity();

        veterinaryBranchOfficeDTO = mock(VeterinaryBranchOfficeDTO.class);
        veterinaryBranchOfficeDTO = instanceEntityVBDTO.createEntity();

        vet = mock(Veterinary.class);
        branchOffice = mock(BranchOffice.class);

        veterinaryOptional = Optional.of(vet);
        branchOfficeOptional = Optional.of(branchOffice);
        veterinaryBranchOfficeDTOOptional = Optional.of(veterinaryBranchOfficeDTO);

        MockitoAnnotations.initMocks(this);

        branchOffice.setAddressBranch(ADDRESS_BRANCH_OFFICE);
        branchOffice.setImageBranch(IMAGE_BRANCH_OFFICE);
        branchOffice.setTelephoneBranch(TELEPHONE_BRANCH_OFFICE);

    }

    @Test
    void findByIdTest() {
        Mockito.when(veterinaryRepository.findById(ID)).thenReturn(veterinaryOptional);
        veterinaryService.findById(ID);
    }

    @Test
    void saveVeterinaryTest() {
        Mockito.when(veterinaryRepository.save(veterinary)).thenReturn(veterinary);
        Veterinary vetSave = veterinaryService.saveVeterinary(veterinary);

        assertNotNull(vetSave);
    }

    @Test
    void updateVeterinaryTest() {
        Mockito.when(veterinaryRepository.save(vet)).thenReturn(vet);
        Veterinary vetUpdate = veterinaryService.updateVeterinary(vet);

        assertNotNull(vetUpdate);
        assertEquals(vet, vetUpdate);
    }

    @Test
    void findByAllTest () {
        final Veterinary veterinary = new Veterinary();
        Mockito.when(veterinaryRepository.findAll()).thenReturn(Arrays.asList(veterinary));
        final List<Veterinary> resp = veterinaryService.findAll();

        assertNotNull(resp);
        assertFalse(resp.isEmpty());
        assertEquals(resp.size(), 1);
    }

    @Test
    void findbyIdVetSucTest () {
        Mockito.when(veterinaryRepository.findById(ID)).thenReturn(veterinaryOptional);
        Mockito.when(branchOfficeRepository.findById(ID_BRANCH_OFFICE)).thenReturn(branchOfficeOptional);
        final Optional<VeterinaryBranchOfficeDTO> resp = veterinaryService.findbyIdVetSuc(ID, ID_BRANCH_OFFICE);

        assertNotNull(resp);
    }

    @Test
    void findAllVetsByStateTest() {
        final int number = 1;
        final Veterinary veterinary = new Veterinary();
        Mockito.when(veterinaryRepository.findAllByStateVet(true)).thenReturn(Arrays.asList(veterinary));
        final List<Veterinary> resp = veterinaryService.findAllByStateVet(true);

        assertNotNull(resp);
        assertFalse(resp.isEmpty());
        assertEquals(resp.size(),number);
    }

    @Test
    void deleteVeterinaryTest() {
        Mockito.when(veterinaryRepository.findById(ID)).thenReturn(veterinaryOptional);
        veterinaryService.deleteVeterinary(ID);
    }

}
