package com.practica.proyecto.controller;

import com.practica.proyecto.factory.EntityFactory;
import com.practica.proyecto.factory.InstanceEntity;
import com.practica.proyecto.model.Appointment;
import com.practica.proyecto.service.AppointmentService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.http.ResponseEntity;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.mock;

class AppointmentControllerTest {
    public final long ID = Long.valueOf(1);
    private static final String HOUR = "13:00";
    private static final String DATE = "10/10/2020";

    private InstanceEntity<Appointment> appointmentIE =EntityFactory.createAppointment();

    private Appointment appointment;
    private Optional<Appointment> appointmentOptional;

    @Mock
    private AppointmentService appointmentService;

    @InjectMocks
    private AppointmentController appointmentController;

    @BeforeEach
    public void init(){
        appointment = mock(Appointment.class);
        appointmentOptional = Optional.of(appointment);

        MockitoAnnotations.initMocks(this);
        appointment.setId(ID);
        appointment.setHour(HOUR);
        appointment.setDate(DATE);
    }

    @Test
    void saveAppointmentTest(){
        Mockito.when(appointmentService.saveAppointment(appointment)).thenReturn(appointment);
        Appointment appointmentSave = appointmentController.saveAppointment(appointment);

        assertNotNull(appointmentSave);
    }

    @Test
    void updateAppointmentTest() {
        Mockito.when(appointmentService.updateAppointment(appointment)).thenReturn(appointment);
        Appointment appointmentUpdate = appointmentController.updateAppointment(appointment);

        assertNotNull(appointmentUpdate);
    }

    @Test
    void findByAllTest () {
        final int number = 1;
        Mockito.when(appointmentService.findAll()).thenReturn(Arrays.asList(appointment));
        final List<Appointment> appointmentList = appointmentController.findAll();


        assertFalse(appointmentList.isEmpty());
        assertEquals(appointmentList.size(), number);
    }

    @Test
    void deleteAppointment() {
        Mockito.when(appointmentService.findById(ID)).thenReturn(appointmentOptional);
        appointmentController.deleteById(ID);
    }

    @Test
    void findByIdTest() {
        Mockito.when(appointmentService.findById(ID)).thenReturn(appointmentOptional);
        ResponseEntity<Appointment> appointmentFind= appointmentController.findById(ID);

        assertNotNull(appointmentFind);
    }
}
