package com.practica.proyecto.controller;

import com.practica.proyecto.converter.VeterinaryConverter;
import com.practica.proyecto.dto.VeterinaryBranchOfficeDTO;
import com.practica.proyecto.dto.VeterinaryDTO;
import com.practica.proyecto.factory.EntityFactory;
import com.practica.proyecto.factory.InstanceEntity;
import com.practica.proyecto.model.BranchOffice;
import com.practica.proyecto.model.Veterinary;
import com.practica.proyecto.service.VeterinaryService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.mock;

class VeterinaryControllerTest {

    private InstanceEntity<Veterinary> veterinaryIE = EntityFactory.createVeterinary();
    private InstanceEntity<VeterinaryDTO> veterinaryDTOInstanceEntity = EntityFactory.createVeterinaryDTO();

    private BranchOffice branchOffice;
    private InstanceEntity<BranchOffice> branchOfficeIE = EntityFactory.createBranchOffice() ;


    private VeterinaryDTO veterinaryDTO;
    private Veterinary veterinary;
    private Veterinary veterinaryResp;
    private Optional<Veterinary> optionalVeterinary;

    private VeterinaryBranchOfficeDTO veterinaryBranchOfficeDTO;
    private Optional<VeterinaryBranchOfficeDTO> optionalVeterinaryBranchOfficeDTO;

    private VeterinaryConverter veterinaryConverter;

    @Mock
    private VeterinaryService veterinaryService;

    @InjectMocks
    private VeterinaryController veterinaryController;

    @BeforeEach
    public void init() {
        veterinary = mock(Veterinary.class);
        veterinary = veterinaryIE.createEntity();
        veterinaryConverter = mock(VeterinaryConverter.class);

        branchOffice = mock(BranchOffice.class);
        branchOffice = branchOfficeIE.createEntity();

        veterinaryDTO = mock(VeterinaryDTO.class);
        veterinaryDTO = veterinaryDTOInstanceEntity.createEntity();

        veterinaryResp = mock(Veterinary.class);
        veterinaryResp = veterinaryConverter.fromDto(veterinaryDTO);


        veterinaryBranchOfficeDTO = mock(VeterinaryBranchOfficeDTO.class);

        optionalVeterinary = Optional.of(veterinary);
        optionalVeterinaryBranchOfficeDTO = Optional.of(veterinaryBranchOfficeDTO);

        MockitoAnnotations.initMocks(this);
    }

   @Test
    void saveVeterinaryTest() {
        Mockito.when(veterinaryService.saveVeterinary(veterinaryResp)).thenReturn(veterinaryResp);
        Veterinary vet = veterinaryController.saveVeterinary(veterinaryDTO);

        assertEquals(veterinaryResp, vet);
    }

    @Test
    void findByIdTest() {
        Mockito.when(veterinaryService.findById(veterinary.getIdVet())).thenReturn(optionalVeterinary);
        ResponseEntity<Veterinary> vet = veterinaryController.findById(veterinary.getIdVet());

        assertNotNull(vet);
    }

    @Test
    void deleteVeterinaryTest() {
        Mockito.when(veterinaryService.findById(veterinary.getIdVet())).thenReturn(optionalVeterinary);
        veterinaryController.deleteVeterinary(veterinary.getIdVet());
    }

    @Test
    void findAllByStateVetTest() {
        final int number = 1;
        final Veterinary veterinary = new Veterinary();
        Mockito.when(veterinaryService.findAllByStateVet(true)).thenReturn(Arrays.asList(veterinary));
        final List<Veterinary> resp = veterinaryController.findAllByStateVet(true);

        assertNotNull(resp);
        assertFalse(resp.isEmpty());
        assertEquals(resp.size(), number);
    }

    @Test
    void findAllTest() {
        final Veterinary veterinary = new Veterinary();
        Mockito.when(veterinaryService.findAll()).thenReturn(Arrays.asList(veterinary));
        final List<Veterinary> resp = veterinaryController.findAll();

        assertNotNull(resp);
        assertFalse(resp.isEmpty());
        assertEquals(1, resp.size());
    }

    @Test
    void updateVeterinaryTest() {
        Mockito.when(veterinaryService.updateVeterinary(veterinaryResp)).thenReturn(veterinaryResp);
        Veterinary veterinaryUpdate = veterinaryController.updateVeterinary(veterinaryDTO);

        assertEquals(veterinaryResp, veterinaryUpdate);
    }


    @Test
    void findByIdVetBranchTest() {
        Mockito.when(veterinaryService.findbyIdVetSuc(veterinary.getIdVet(),branchOffice.getIdBranch() )).
                thenReturn(optionalVeterinaryBranchOfficeDTO);
        final ResponseEntity<VeterinaryBranchOfficeDTO> resp =
                veterinaryController.findbyIdVetSuc(veterinary.getIdVet(), branchOffice.getIdBranch());

        assertNotNull(resp);
        assertEquals(HttpStatus.OK, resp.getStatusCode());
    }


}
