package com.practica.proyecto.controller;

import com.practica.proyecto.dto.UserDTO;
import com.practica.proyecto.factory.EntityFactory;
import com.practica.proyecto.factory.InstanceEntity;
import com.practica.proyecto.model.User;
import com.practica.proyecto.service.UserService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.mock;

class UserControllerTest {

    private User user;
    private InstanceEntity<User> userInstanceEntity = EntityFactory.createUser();
    private Optional<User> optionalUserController;
    private UserDTO userDTO;
    private InstanceEntity<UserDTO> userDTOInstanceEntity = EntityFactory.createUserDTO();


    @Mock
    private UserService userService;

    @InjectMocks
    private UserController userController;

    @BeforeEach
    public void init(){
        user = mock(User.class);
        user = userInstanceEntity.createEntity();

        userDTO = mock(UserDTO.class);
        userDTO = userDTOInstanceEntity.createEntity();

        optionalUserController = Optional.of(user);
        MockitoAnnotations.initMocks(this);
    }

    @Test
    void saveUserTest(){
        Mockito.when(userService.saveUser(user)).thenReturn(user);
        User userSave = userController.saveUser(userDTO);

    }

    @Test
    void findByIdTest(){
        Mockito.when(userService.findById(user.getId())).thenReturn(optionalUserController);
        Optional<User> userFindById = this.userController.findById(user.getId());

        assertNotNull(userFindById);
    }

    @Test
    void updateUserTest(){
        Mockito.when(userService.updateUser(user)).thenReturn(user);
        User userUpdate = userController.updateUser(userDTO);

    }


    @Test
    void allActiveTest(){
        final User user = new User();
        Mockito.when(userService.findAllByState(true)).thenReturn(Arrays.asList(user));
        final List<User> resp = this.userController.findAllByState(true);

        assertNotNull(resp);
        assertFalse(resp.isEmpty());
    }

    @Test
    void allTest(){
        final User user = new User();
        Mockito.when(userService.findAll()).thenReturn(Arrays.asList(user));
        final List<User> resp = this.userController.findAll();

        assertNotNull(resp);
        assertFalse(resp.isEmpty());
    }

    @Test
    void deleteTest(){
        Mockito.when(userService.findById(user.getId())).thenReturn(optionalUserController);
        userController.deleteById(user.getId());
    }
}
