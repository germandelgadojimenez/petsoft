package com.practica.proyecto.controller;

import com.practica.proyecto.dto.PetDTO;
import com.practica.proyecto.factory.EntityFactory;
import com.practica.proyecto.factory.InstanceEntity;
import com.practica.proyecto.model.Pet;
import com.practica.proyecto.service.PetService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.mock;

class PetControllerTest {


    private Pet pet;
    private InstanceEntity<Pet> petInstanceEntity = EntityFactory.createPet();
    private  Optional<Pet> optionalPetController;
    private PetDTO petDTO;
    private InstanceEntity<PetDTO> petDtoInstanceEntity = EntityFactory.createPetDTO();


    @Mock
    private PetService petService;

    @InjectMocks
    PetController petController;

    @BeforeEach
    public void init(){
        pet = mock(Pet.class);
        pet = petInstanceEntity.createEntity();

        petDTO = mock(PetDTO.class);
        petDTO = petDtoInstanceEntity.createEntity();

        optionalPetController = Optional.of(pet);
        MockitoAnnotations.initMocks(this);
    }

    @Test
    void savePetTest() {
        Mockito.when(petService.savePet(pet)).thenReturn(pet);
        Pet petSave = petController.savePet(petDTO);
    }

  /*
    @Test
    void findByIdTest() {
        Mockito.when(petService.findById(pet.getIdPet())).thenReturn(optionalPetController);
        Optional<Pet> petFindById = this.petController.findById(pet.getIdPet());
        assertNotNull(petFindById);
    }
*/

    @Test
    void deletePetTest() {
        Mockito.when(petService.findById(pet.getIdPet())).thenReturn(optionalPetController);
        petController.deleteMascota(pet.getIdPet());
    }

    @Test
    void findAllByStatePetTest() {
        final Pet pet = new Pet();
        Mockito.when(petService.findAllByStatePet(true)).thenReturn(Arrays.asList(pet));
        final List<Pet> resp = petController.findAllByStatePet(true);

        assertNotNull(resp);
        assertFalse(resp.isEmpty());
        assertEquals(resp.size(), 1);
    }

    @Test
    void findAllTest() {
        final Pet pet = new Pet();
        final int number = 1;
        Mockito.when(petService.findAll()).thenReturn(Arrays.asList(pet));
        final List<Pet> resp = petController.findAll();

        assertNotNull(resp);
        assertFalse(resp.isEmpty());
        assertEquals(resp.size(),number);
    }

    @Test
    void updatePetTest() {
        Mockito.when(petService.updatePet(pet)).thenReturn(pet);
        Pet petUpdate = petController.updatePet(petDTO);
    }
}
