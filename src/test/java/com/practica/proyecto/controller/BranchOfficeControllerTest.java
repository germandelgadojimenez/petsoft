package com.practica.proyecto.controller;

import com.practica.proyecto.model.BranchOffice;
import com.practica.proyecto.service.BranchOfficeService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.mock;

class BranchOfficeControllerTest {

    private static final Long ID_BRANCH_OFFICE = Long.valueOf(1);
    private static final String ADDRESS_BRANCH_OFFICE = "calle busquela";
    private static final String TELEPHONE_BRANCH_OFFICE = "31454";
    private static final String IMAGE_BRANCH_OFFICE = "LOL.PNG";

    private BranchOffice branchOffice;
    private Optional<BranchOffice> optionalBranchOffice;

    @Mock
    private BranchOfficeService branchOfficeService;

    @InjectMocks
    private BranchOfficeController branchOfficeController;

    @BeforeEach
    public void init() {
        branchOffice = mock(BranchOffice.class);
        optionalBranchOffice = Optional.of(branchOffice);

        MockitoAnnotations.initMocks(this);
        branchOffice.setIdBranch(ID_BRANCH_OFFICE);
        branchOffice.setAddressBranch(ADDRESS_BRANCH_OFFICE);
        branchOffice.setTelephoneBranch(TELEPHONE_BRANCH_OFFICE);
        branchOffice.setImageBranch(IMAGE_BRANCH_OFFICE);
    }

    @Test
    void saveSucurtest() {
        Mockito.when(branchOfficeService.saveBranchOffice(this.branchOffice)).thenReturn(this.branchOffice);
        BranchOffice branchOffice = branchOfficeController.saveBranchOffice(this.branchOffice);

        assertEquals(this.branchOffice, branchOffice);
        assertNotNull(branchOffice);
    }

    @Test
    void findAllTest() {
        final int number = 1;
        final BranchOffice branchOffice = new BranchOffice();
        Mockito.when(branchOfficeService.findAll()).thenReturn(Arrays.asList(branchOffice));
        final List<BranchOffice> resp = branchOfficeController.findAll();

        assertNotNull(resp);
        assertFalse(resp.isEmpty());
        assertEquals(resp.size(),number);
    }

    @Test
    void updateSucursalTest() {
        Mockito.when(branchOfficeService.updateBranchOffice(branchOffice)).thenReturn(branchOffice);
        BranchOffice branchOffice = branchOfficeController.updateBranchOffice(this.branchOffice);

        assertNotNull(branchOffice);
        assertEquals(this.branchOffice, branchOffice);
    }

    @Test
    void deleteSucursalTest() {
        Mockito.when(branchOfficeService.findById(ID_BRANCH_OFFICE)).thenReturn(optionalBranchOffice);
        branchOfficeController.deleteBranchOffice(ID_BRANCH_OFFICE);
    }

    @Test
    void findByIdTest() {
        Mockito.when(branchOfficeService.findById(ID_BRANCH_OFFICE)).thenReturn(optionalBranchOffice);
        Optional<BranchOffice> suc = branchOfficeController.findById(ID_BRANCH_OFFICE);

        assertNotNull(suc);
    }
}
